# Simple Hi Lo Card Game App

## The Rules

1. Play consists of a dealer and a player.
2. The dealer draws a card from the top of the deck by and places it face up. This happens automatically when the game starts or resets, otherwise the dealer player clicks the Dealer Draw button.
3. The player (incicated by the Turn field) must guess whether the next card drawn from the deck will be higher or lower than the face up card. The player click either Guess High or Guess Low.
4. Once the player guesses, the next card is automatically drawn and placed face up on top of the previous card.
5. If the player is correct (equal card values count as correct), go back to step 2.
6. If the player is wrong, the player receives a point for each card in the face up pile, and the face up pile is discarded. Then play begins at step 1 again.

When the player has made three correct guesses in a row, s/he may make another guess, or choose to pass by clicking the Pass button and the roles are reversed with the face up pile continuing to build. The player may choose to continue if there is a high likelihood that their next guess would be correct. If they are wrong, play starts over at step 1 and the player must again make three correct guesses before being allowed to pass. If they are correct, they can continue or pass. The goal is to end the game with as few points as possible.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
