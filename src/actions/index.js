// Helper functions

// Get numerical value of card for comparisons
const getCardNumeValue = cardValue => {
  switch (cardValue) {
    case "JACK":
      return 11

    case "QUEEN":
      return 12

    case "KING":
      return 13

    case "ACE":
      // Treat Aces as the lowest value
      return 1

    default:
      // Convert number card values to integers
      return parseInt(cardValue, 10)
  }
}

// Sync actions

export const REQUEST_DECK = 'REQUEST_DECK'
export const requestDeck = () => dispatch => {
  dispatch({
    type: REQUEST_DECK
  })
}

export const RECEIVED_DECK = 'RECEIVED_DECK'
export const receivedDeck = newGame => dispatch => {
  dispatch({
    type: RECEIVED_DECK,
    ...newGame
  })
}

export const REQUEST_GUESS = 'REQUEST_GUESS'
export const requestGuess = () => dispatch => {
  dispatch({
    type: REQUEST_GUESS
  })
}

export const RECEIVED_GUESS = 'RECEIVED_GUESS'
export const receivedGuess = gameUpdate => dispatch => {
  dispatch({
    type: RECEIVED_GUESS,
    ...gameUpdate
  })
}

export const REQUEST_DRAW = 'REQUEST_DRAW'
export const requestDraw = () => dispatch => {
  dispatch({
    type: REQUEST_DRAW
  })
}

export const RECEIVED_DRAW = 'RECEIVED_DRAW'
export const receivedDraw = gameUpdate => dispatch => {
  dispatch({
    type: RECEIVED_DRAW,
    ...gameUpdate
  })
}

export const PASS_TURN = 'PASS_TURN'
export const passTurn = () => (dispatch, getState) => {
  const { turn } = getState()
  dispatch({
    type: PASS_TURN,
    turn: turn === 1 ? 2 : 1,
    streak: 0
  })
}

// Async actions

const endpointPrefix = 'https://deckofcardsapi.com/api/deck/'

export const startNewGame = () => (dispatch) => {
  let thisDeckId = 0
  let cardImage = ''
  let cardValue = 0

  dispatch(requestDeck())

  // Init the deck
  return fetch(`${endpointPrefix}new/shuffle/?deck_count=1`)
    .then(response => response.json())
    .then(json => {
      // Draw the first card
      thisDeckId = json.deck_id
      return fetch(`${endpointPrefix}${thisDeckId}/draw/?count=1`)
    })
    .then(response => response.json())
    .then(json => {
      // Add card to face up pile
      const {
        code,
        image,
        value
      } = json.cards[0]
      cardImage = image
      cardValue = value
      return fetch(
        `${endpointPrefix}${thisDeckId}/pile/faceUp/add/?cards=${code}`
      )
    })
    .then(response => response.json())
    .then(({ piles: { faceUp }, remaining }) => {
      dispatch(receivedDeck({
        deckId: thisDeckId,
        cardImage,
        cardValue,
        faceUpCount: faceUp.remaining,
        faceDownCount: remaining,
        lastGuess: false,
        player1Score: 0,
        player2Score: 0,
        streak: 0,
        turn: 1
      }))
    })
    .catch(error => dispatch(
      receivedDeck({ error: `Failed to make guess. Error: ${error}`})
    ))
}

export const dealerDraw = () => (dispatch, getState) => {
  const { deckId } = getState()

  let newCardImage = ''
  let newCardValue = 0

  dispatch(requestDraw())

  // Draw the next card
  return fetch(`${endpointPrefix}${deckId}/draw/?count=1`)
    .then(response => response.json())
    .then(json => {
      // Add card to face up pile
      const {
        code,
        image,
        value
      } = json.cards[0]
      newCardImage = image
      newCardValue = value
      return fetch(
        `${endpointPrefix}${deckId}/pile/faceUp/add/?cards=${code}`
      )
    })
    .then(response => response.json())
    .then(json => {
      const {
        piles: { faceUp },
        remaining
      } = json

      dispatch(receivedDraw({
        cardImage: newCardImage,
        cardValue: newCardValue,
        dealerCanDraw: false,
        faceDownCount: remaining,
        faceUpCount: faceUp.remaining,
        lastGuess: false,
      }))
    })
    .catch(error => dispatch(
      receivedDraw({ error: `Failed to make guess. Error: ${error}`})
    ))
}

export const GUESS_HIGH = 'GUESS_HIGH'
export const GUESS_LOW = 'GUESS_LOW'
export const GUESS_CORRECT = 'GUESS_CORRECT'
export const GUESS_INCORRECT = 'GUESS_INCORRECT'
export const makeGuess = (guess) => (dispatch, getState) => {
  const {
    cardValue,
    deckId,
    faceUpCount,
    faceDownCount,
    player1Score,
    player2Score,
    streak,
    turn,
  } = getState()

  let newCardImage = ''
  let newCardValue = 0
  let newStreak = streak
  let newLastGuess = GUESS_INCORRECT
  let newPlayer1Score = player1Score
  let newPlayer2Score = player2Score
  let newFaceDownCount = faceDownCount
  let newFaceUpCount = faceUpCount

  dispatch(requestGuess())

  // Draw the next card
  return fetch(`${endpointPrefix}${deckId}/draw/?count=1`)
    .then(response => response.json())
    .then(json => {
      // Check guess and add card to face up pile
      const {
        code,
        image,
        value
      } = json.cards[0]

      newCardImage = image
      newCardValue = value
      newFaceDownCount = json.remaining

      // For this game ties are considered sucessfull guesses.
      if (
        (
          guess === GUESS_HIGH &&
          getCardNumeValue(newCardValue) >= getCardNumeValue(cardValue)
        ) ||
        (
          guess === GUESS_LOW &&
          getCardNumeValue(newCardValue) <= getCardNumeValue(cardValue)
        )
      ) {
        // Correct guess
        newStreak += 1
        newLastGuess = GUESS_CORRECT
      } else {
        // Incorrect guess
        newStreak = 0
      }

      return fetch(`${endpointPrefix}${deckId}/pile/faceUp/add/?cards=${code}`)
    })
    .then(response => response.json())
    .then(json => {
      const { piles: {faceUp}} = json
      if (newLastGuess === GUESS_INCORRECT) {
        // Handle incorrect guess
        newFaceUpCount = 0
        if (turn === 1) {
          newPlayer1Score = newPlayer1Score + faceUp.remaining
        } else {
          newPlayer2Score = newPlayer2Score + faceUp.remaining
        }

        return fetch(`${endpointPrefix}${deckId}/pile/faceUp/draw/?count=${faceUpCount}`)
          .then(response => response.json())
      }

      // Handle correct guess
      newFaceUpCount = faceUp.remaining
      return new Promise(resolve => resolve(json))
    })
    .then(json => {
      dispatch(receivedGuess({
        cardImage: newCardImage,
        cardValue: newCardValue,
        dealerCanDraw: true,
        faceDownCount: newFaceDownCount,
        faceUpCount: newFaceUpCount,
        lastGuess: newLastGuess,
        player1Score: newPlayer1Score,
        player2Score: newPlayer2Score,
        streak: newStreak
      }))
    })
    .catch(error => dispatch(
      receivedDeck({ error: `Failed to make guess. Error: ${error}`})
    ))
}
