import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import LoadingScreen from 'react-loading-screen'
import logo from './logo.svg'
import './App.css'
import {
  startNewGame,
  dealerDraw,
  makeGuess,
  passTurn,
  GUESS_HIGH,
  GUESS_LOW,
  GUESS_CORRECT
} from './actions'

const mapStateToProps = state => {
  return state
}

const mapDispatchToProps = dispatch => ({
  startNewGame: () => dispatch(startNewGame()),
  dealerDraw: () => dispatch(dealerDraw()),
  makeGuess: (guess) => dispatch(makeGuess(guess)),
  passTurn: () => dispatch(passTurn())
})

const App = (props) => {
  const {
    cardImage,
    dealerCanDraw,
    deckId,
    error,
    faceDownCount,
    faceUpCount,
    isFetching: isFetchingDeck,
    lastGuess,
    player1Score,
    player2Score,
    streak,
    turn,
  } = props

  const showError = error ? true : false
  const showGameStart = !deckId
  const showGame = !showGameStart && faceDownCount > 0
  const showGameFinish = !showGameStart && faceDownCount === 0
  const turnPlayer = turn === 1 ? 'Player 1' : 'Player 2'
  const showGuess = Boolean(lastGuess)
  const guessResult = lastGuess === GUESS_CORRECT ? 'Correct' : 'Incorrect'
  const showCard = Boolean(cardImage)

  let winningMessage = ''
  if (showGameFinish) {
    if (player1Score > player2Score) {
      winningMessage = `Player 2 wins!`
    } else if (player1Score < player2Score) {
      winningMessage = `Player 1 wins!`
    } else {
      winningMessage = `It's a tie!`
    }
  }
  console.log()
  return (
    <div className="App">
      <LoadingScreen
        loading={isFetchingDeck}
        bgColor='#FFFFFF'
        spinnerColor='#9ee5f8'
        textColor='#676767'
        logoSrc={logo}
      >
        {showError && <div>{error}</div>}
        {!showError && (
          <Fragment>
            <h1 className="App-title">Hi Lo</h1>
            {showGameStart && <button onClick={props.startNewGame}>Start Game</button>}
            {showGame && (
              <Fragment>
                <div>Turn: {turnPlayer}</div>
                <div>Streak: {streak}</div>
                <div>Cards remaining in deck: {faceDownCount}</div>
                <div>Cards in face up pile: {faceUpCount}</div>
                {showCard && <div><img src={cardImage} alt="card"/></div>}
                <div>Player 1 score: {player1Score}</div>
                <div>Player 2 score: {player2Score}</div>
                {showGuess && <div>Guess: {guessResult}</div>}
                <button
                  onClick={() => props.makeGuess(GUESS_HIGH)}
                  disabled={dealerCanDraw}
                >
                  Guess High
                </button>
                <button
                  onClick={() => props.makeGuess(GUESS_LOW)}
                  disabled={dealerCanDraw}
                >
                  Guess Low
                </button>
                <br />
                <button
                  onClick={() => props.dealerDraw()}
                  disabled={!dealerCanDraw}
                >
                  Dealer Draw
                </button>
                <button
                  disabled={streak < 3}
                  onClick={() => props.passTurn()}
                >
                  Pass
                </button>
                <br />
                <button onClick={() => props.startNewGame(deckId)}>Reset Game</button>
              </Fragment>
            )}
            {showGameFinish && <Fragment>
              <div>{winningMessage}</div>
              <button onClick={() => props.startNewGame(deckId)}>Start New Game</button>
            </Fragment>}
          </Fragment>
        )}

      </LoadingScreen>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
