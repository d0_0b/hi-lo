import {
  REQUEST_DECK,
  RECEIVED_DECK,
  REQUEST_GUESS,
  RECEIVED_GUESS,
  REQUEST_DRAW,
  RECEIVED_DRAW,
  PASS_TURN
} from '../actions'

export const handleGame = (state = {
  cardImage: false,
  cardValue: 0,
  dealerCanDraw: false,
  deckId: false,
  error: false,
  faceDownCount: 52,
  faceUpCount: 0,
  gameStarted: false,
  isFetching: false,
  lastGuess: false,
  player1Score: 0,
  player2Score: 0,
  streak: 0,
  turn: 1,
}, action) => {
  console.log('state', state)
  console.log('action', action)

  if (action.error) {
    return {
      ...state,
      isFetching: false,
      error: action.error,
    }
  }

  switch (action.type) {
    case REQUEST_DECK:
    case REQUEST_GUESS:
    case REQUEST_DRAW:
      return {
        ...state,
        dealerCanDraw: false,
        isFetching: true,
        error: false,
      }

    case PASS_TURN:
      return {
        ...state,
        turn: action.turn,
        streak: action.streak
      }

    case RECEIVED_DECK:
      if (action.error) {
        return {
          ...state,
          isFetching: false,
          error: action.error
        }
      }
      return {
        ...state,
        gameStarted: true,
        isFetching: false,
        cardImage: action.cardImage,
        cardValue: action.cardValue,
        deckId: action.deckId,
        faceDownCount: action.faceDownCount,
        faceUpCount: action.faceUpCount,
        lastGuess: action.lastGuess,
        player1Score: action.player1Score,
        player2Score: action.player2Score,
        streak: action.streak,
        turn: action.turn
      }

    case RECEIVED_GUESS:
      return {
        ...state,
        isFetching: false,
        cardImage: action.cardImage,
        cardValue: action.cardValue,
        dealerCanDraw: action.dealerCanDraw,
        faceDownCount: action.faceDownCount,
        faceUpCount: action.faceUpCount,
        lastGuess: action.lastGuess,
        player1Score: action.player1Score,
        player2Score: action.player2Score,
        streak: action.streak
      }

    case RECEIVED_DRAW:
      return {
        ...state,
        isFetching: false,
        cardImage: action.cardImage,
        cardValue: action.cardValue,
        dealerCanDraw: action.dealerCanDraw,
        faceDownCount: action.faceDownCount,
        faceUpCount: action.faceUpCount,
        lastGuess: action.lastGuess
      }

    default:
      return state
  }
}
